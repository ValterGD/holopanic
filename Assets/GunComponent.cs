﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "gun_", menuName = "Holo Panic/Combat/Gun Component", order = 0)]
public class GunComponent : BaseComponent
{
	[Header("Ammunition")]
	public Ammunition ammo;

	[Header("Mechanism")]
	public int roundsPerSecond;
	public bool holdBased;
	[Range(0,1)] public float precision;

	private void Awake()
	{
		componentType = ComponentType.gun;
	}

	public void AngledShoot(Vector3 origin, float angle)
	{
		ammo.pool.SpwObject(origin, Quaternion.Euler(0, angle, 0));
	}
}
