﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

[RequireComponent(typeof(CinemachineVirtualCamera))]
public class VirtualCameraShaker : MonoBehaviour
{
	public static VirtualCameraShaker instance;

	private CinemachineVirtualCamera vCam;
	private CinemachineBasicMultiChannelPerlin perlin;

	[SerializeField] private float shakeDuration = 0.05f;
	[SerializeField] private float shakeAmplitude = 1;

	private void Awake()
	{
		if (instance != null)
			Debug.LogError("Dois shakes definidos no projeto");

		instance = this;

		vCam = GetComponent<CinemachineVirtualCamera>();
		perlin = vCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
	}

	public void ShakeCamera()
	{
		StopAllCoroutines();
		StartCoroutine(CameraShaking());
	}

	private IEnumerator CameraShaking()
	{
		perlin.m_AmplitudeGain = shakeAmplitude;
		yield return new WaitForSeconds(shakeDuration);
		perlin.m_AmplitudeGain = 0;
	}
}
