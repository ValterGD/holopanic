﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
	public static AudioManager instance;

	private AudioSource shoot2dsource;

	private void Awake()
	{
		instance = this;

		shoot2dsource = gameObject.AddComponent<AudioSource>();
		shoot2dsource.playOnAwake = false;
		shoot2dsource.spatialBlend = 0;
	}

	public static void ShootAudio(CustomAudioClip customClip)
	{
		ShootAudio(customClip.audioClip, customClip.volume);
	}

	public static void ShootAudio(AudioClip clip, float vol)
	{
		instance.shoot2dsource.PlayOneShot(clip, vol);
	}

}
