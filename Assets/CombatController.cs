﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatController : MonoBehaviour
{
	private Dictionary<Direction, IEnumerator> gunsOnFire = new Dictionary<Direction, IEnumerator>();
	private Rigidbody rb;

	private void Awake()
	{
		rb = GetComponent<Rigidbody>();
	}

	public void StartShooting(GunComponent gun, Direction direction)
	{
		if (gun.holdBased)
		{
			IEnumerator mGun = MachineGunner(gun, DirectionData.MakeData(direction));
			gunsOnFire.Add(direction, mGun);
			StartCoroutine(mGun);
		}
		else
		{
			gun.AngledShoot(transform.position, DirectionData.DirectionToAngle(direction));
		}
	}

	public void StopShooting(Direction direction)
	{
		if (gunsOnFire.ContainsKey(direction))
		{
			StopCoroutine(gunsOnFire[direction]);
			gunsOnFire.Remove(direction);
		}
	}

	private IEnumerator MachineGunner(GunComponent gun, DirectionData dir)
	{
		do
		{
			float precisionAngle = dir.angle + (Random.Range(-45f, 45f) * (1f - gun.precision));
			gun.AngledShoot(transform.position + (Vector3.up * 0.99f) + (dir.vector * 0.5f), precisionAngle);
			rb.MovePosition(transform.position + (-dir.vector * 0.05f));
			yield return new WaitForSeconds(1f / gun.roundsPerSecond);
		} while (true);

	}
}
