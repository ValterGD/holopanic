﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundsShootLimits : MonoBehaviour
{
	private void OnTriggerExit(Collider other)
	{
		if (other.gameObject.CompareTag("Projectile"))
		{
			other.gameObject.SetActive(false);
		}
	}
}
