﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Direction { NaN, North, South, West, East }

public struct DirectionData
{
	public DirectionData(Direction dir)
	{
		angle = DirectionToAngle(dir);
		vector = DirectionToVector(dir);
	}

	public float angle;
	public Vector3 vector;

	public static int DirectionToAngle(Direction dir)
	{
		switch (dir)
		{
			case Direction.West:
				return -90;
			case Direction.East:
				return 90;
			case Direction.South:
				return 180;
			default:
				return 0;
		}
	}

	public static Vector3 DirectionToVector(Direction dir)
	{
		switch (dir)
		{
			case Direction.North:
				return Vector3.forward;
			case Direction.South:
				return Vector3.back;
			case Direction.West:
				return Vector3.left;
			default:
				return Vector3.right;
		}
	}

	public static DirectionData MakeData(Direction dir)
	{
		return new DirectionData(dir);
	}

}
