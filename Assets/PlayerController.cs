﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
	private AngledMovimentSystem angledMoviment;

	private float inputAngle;
	[SerializeField] private float vertAxis;
	[SerializeField] private float horAxis;

	private void Awake()
	{
		angledMoviment = GetComponent<AngledMovimentSystem>();
	}

	private void Update()
	{
		MovePlayerAtAngle();
		vertAxis = Input.GetAxis("Vertical");
		horAxis = Input.GetAxis("Horizontal");
	}

	public void MovePlayerAtAngle()
	{
		if (Input.GetAxisRaw("Vertical") != 0 || Input.GetAxisRaw("Horizontal") != 0)
		{
			inputAngle = Mathf.Atan2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) * Mathf.Rad2Deg;

			angledMoviment.MoveAtAngle(inputAngle);
		}
	}

}