﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestRotationByInput : MonoBehaviour
{
	float angle;

	private void FixedUpdate()
	{
		angle = Mathf.Atan2(Input.GetAxis("Vertical"), Input.GetAxis("Horizontal")) * Mathf.Rad2Deg;

		transform.localEulerAngles = new Vector3(0, angle, 0);

		float mag = Mathf.Clamp01(new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")).magnitude);

		Debug.Log(mag);
	}
}
