﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class AmmoPool : MonoBehaviour
{
	[SerializeField] Ammunition ammo;
	[SerializeField] ObjectPool pool;

	private void OnEnable()
	{
		//if (ammo.pool == null)
		{
			ammo.pool = pool;
		}
		//else
		{
			//Debug.LogError("Systema de munição já definido em: " + ammo.pool.gameObject.name);
		}
	}

	private void OnDisable()
	{
		//if (ammo.pool == this)
		{
			ammo.pool = null;
		}
		//else
		{
		//	Debug.LogError("Systema de munição já definido");
		}
	}
}
