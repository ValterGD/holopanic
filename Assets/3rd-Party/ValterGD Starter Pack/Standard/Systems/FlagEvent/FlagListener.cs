﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FlagListener : MonoBehaviour {

	[SerializeField] protected FlagEvent flag;
	[SerializeField] protected UnityEvent gameEvent;

	private void OnEnable()
	{
		try
		{
			flag.AddListener(this);
		}
		catch
		{
			Debug.LogWarning(gameObject.name + " have no flag to listen!");
		}
	}

	private void OnDisable()
	{
		try
		{
			flag.RemoveListener(this);
		}
		catch
		{
			Debug.LogWarning(gameObject.name + " have no flag to listen!");
		}
	}

	public virtual void ExecuteEvent() {
		gameEvent.Invoke();
	}

}
