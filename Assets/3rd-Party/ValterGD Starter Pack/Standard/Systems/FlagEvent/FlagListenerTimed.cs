﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FlagListenerTimed : FlagListener {

	[SerializeField] private float timeToEvent;

	public override void ExecuteEvent()
	{
		Invoke("InvokeEvent", timeToEvent);
	}

	private void InvokeEvent() {
		gameEvent.Invoke();
	}

}
