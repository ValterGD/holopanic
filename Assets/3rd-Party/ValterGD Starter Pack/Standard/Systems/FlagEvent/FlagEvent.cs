﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "flag_", menuName = "ValterGD's System/Flag Events/Flag")]
public class FlagEvent : ScriptableObject {

	private List<FlagListener> listeners = new List<FlagListener>();

	public void AddListener(FlagListener listener) {
		listeners.Add(listener);
	}

	public void RemoveListener(FlagListener listener) {
		listeners.Remove(listener);
	}

	public void Raise() {
		//Debug.Log("Flag " + this.name + " as Raised!");
		foreach (FlagListener listener in listeners) {
			listener.ExecuteEvent();
			//Debug.Log(name +" Raised!");
		}
	}

}
