﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPoolableObject
{
	void OnSpawned();
	void Unspawn();
}

public interface IPoolableComplex: IPoolableObject
{
	void OnCreated();
	void vanishTimerStarts(float time);
}