﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class PoolableObject : MonoBehaviour, IPoolableObject
{
	protected ObjectPool originalPool;

	[Header("PoolableObject")]
	[SerializeField] private UnityEvent OnSpawnedEvent;

	private void Awake()
	{
		originalPool = transform.parent.GetComponent<ObjectPool>();
	}

	public virtual void OnCreated()
	{
		gameObject.SetActive(false);
	}

	public virtual void OnSpawned()
	{
		OnSpawnedEvent.Invoke();
	}

	public virtual void OnUnspawn()
	{
		StopAllCoroutines();
	}

	public virtual void Unspawn()
	{
		OnUnspawn();
		gameObject.SetActive(false);
	}

	public virtual void vanishTimerStarts(float time)
	{
		StartCoroutine(vanishTimer(time));
	}

	public virtual IEnumerator vanishTimer(float time)
	{
		yield return new WaitForSeconds(time);
		Unspawn();
	}
}
