﻿//ValterGD'S GenericPool v1.0

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour {

	//Classe básica de elementos dentro da pool
	class QueueElement {

		public QueueElement(GameObject gObject, IPoolableObject script, bool cInstance)
		{
			gameObject = gObject;
			pooableScript = script;
			complexInstance = cInstance;
		}

		public GameObject gameObject;
		public IPoolableObject pooableScript;
		public bool complexInstance;
	}

	private Queue<QueueElement> queue;

	[SerializeField] private GameObject instanceObject;
	[SerializeField] private bool layerEqualThis;
	[SerializeField] private int _startQueueQnt = 10;
	[SerializeField] private bool expansive = false;
	[SerializeField] private bool vanishDrivedByObject;
	[SerializeField][Tooltip("Apenas se não for o próprio objeto que faz desaparecer. Valor 0 ou menor faz o objeto nunca desaparecer pelo tempo")]
	private float vanishTime = 3;
	[SerializeField] private bool generateOnAwake = true;

	public int startQueueQnt
	{
		get
		{
			return _startQueueQnt;
		}

		set
		{
			_startQueueQnt = value;
		}
	}

	private void Start()
	{
		if (generateOnAwake)
			GeneratePool();
	}

	public void GeneratePool()
	{
		if (instanceObject == null)
		{
			Debug.LogError("Você está tentando criar uma pool nula em: " + gameObject.name);
			return;
		}

		if (!vanishDrivedByObject && vanishTime <= 0 && expansive) {
			Debug.LogError("A pool em " + gameObject.name + " não faz sentido! Está setada como expansiva mas os objetos nunca desaparencem!");
			return;
		}

		queue = new Queue<QueueElement>();

		for (int i = 0; i < _startQueueQnt; i++)
		{
			EnqueueElement(CreateNewQueueElement());
		}
	}

	private QueueElement CreateNewQueueElement() {
		GameObject instantiatedObj;
		IPoolableObject instanceObjScript;
		bool complexInstance = false;

		instantiatedObj = Instantiate(instanceObject, transform);
		instantiatedObj.transform.parent = transform;
		

		if(layerEqualThis)
		foreach (Transform tf in instantiatedObj.GetComponentsInChildren<Transform>(true))
		{
			tf.gameObject.layer = gameObject.layer;
		}

		//debug.Log(spwLayer.value);

		if ((instanceObjScript = instantiatedObj.GetComponent<IPoolableObject>()) != null)
		{
			if (instanceObjScript.GetType() == typeof(IPoolableComplex))
			{
				complexInstance = true;
				(instanceObjScript as IPoolableComplex).OnCreated();
			}
		}

		QueueElement newQueueElement = new QueueElement(instantiatedObj, instanceObjScript, complexInstance);
		return newQueueElement;
	}

	private void EnqueueElement(QueueElement newQueueElement) {
		newQueueElement.gameObject.SetActive(false);
		queue.Enqueue(newQueueElement);
	}

	public GameObject SpwObject()
	{
		return SpwObject(Vector3.zero, Quaternion.identity, transform);
	}

	public GameObject SpwObject(Transform parent)
	{
		return SpwObject(Vector3.zero, Quaternion.identity, parent);
	}

	public GameObject SpwObject(Vector3 position, Quaternion rotation)
	{
		return SpwObject(position, rotation, transform);
	}

	public GameObject SpwObject(Vector3 position, Quaternion rotation, Transform parent)
	{
		QueueElement SpawedElement;

		if (queue.Peek().gameObject.activeInHierarchy && expansive)
		{
			SpawedElement = CreateNewQueueElement();
		}
		else
		{
			SpawedElement = queue.Dequeue();
			SpawedElement.gameObject.SetActive(true);
		}

		SpawedElement.gameObject.transform.position = position;
		SpawedElement.gameObject.transform.rotation = rotation;
		SpawedElement.gameObject.transform.parent = parent;

		if (SpawedElement.pooableScript != null)
		{
			SpawedElement.pooableScript.OnSpawned();

			if (SpawedElement.complexInstance && !vanishDrivedByObject && vanishTime > 0) {
				(SpawedElement.pooableScript as IPoolableComplex).vanishTimerStarts(vanishTime);
			}
		}

		queue.Enqueue(SpawedElement);

		return SpawedElement.gameObject;
	}

}
