﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectGenerator : MonoBehaviour {

	[Header("Generator Configurations")]
	[SerializeField] private ObjectPool objectToGenerate;
	[SerializeField] private float delayToGenerate = 3f;
	[SerializeField] private bool loop = true;

	[Space]
	[SerializeField] private bool startOnEnable = true;

	[Header("Copies Configurations")]
	[SerializeField] private int copiesToGenerate = 1;
	[SerializeField] private float copiesInterval = 0.25f;

	[Header("Spawn Area")]
	[SerializeField] private Transform spawnableAreasParent;
	[SerializeField] private List<Transform> spawnableAreas;
	[SerializeField] private bool useParetToGenerateList = true;

	[Header("Spawn Random")]
	[SerializeField] private bool filteredRandom = true;
	[SerializeField][Tooltip("Valores negativos significam que um spw só se repetira se todos os outros forem usados")]
	private int spawnsToNextRepete = 0;

	private int nextRepeteCount;

	private List<Transform> openList;

	private float originalGInterval;
	private float gInterval;

	private Vector3 spawnPosition
	{
		get
		{
			Transform returnValue;

			if (filteredRandom && (spawnsToNextRepete >= 0 && (nextRepeteCount >= spawnsToNextRepete)))
			{
				if (openList.Count == 0)
				{
					SetOpenList();
				}

				returnValue = openList[Random.Range(0, openList.Count)];
				openList.Remove(returnValue);
			}
			else
			{
				returnValue = spawnableAreas[Random.Range(0, spawnableAreas.Count)];
			}

			return returnValue.transform.position;
		}
	}

	private void Awake()
	{

		gInterval = delayToGenerate;

		if (useParetToGenerateList)
		{
			spawnableAreas = new List<Transform>();

			for (int i = 0; i < spawnableAreasParent.transform.childCount; i++)
			{
				spawnableAreas.Add(spawnableAreasParent.transform.GetChild(i));
			}
		}

		openList = new List<Transform>();

		if (filteredRandom)
			SetOpenList();
	}

	private void OnEnable()
	{
		if (startOnEnable)
		{
			StartGenerator();
		}
	}

	private void OnDisable()
	{
		StopAllCoroutines();
	}

	public void StartGenerator()
	{
		StartCoroutine(Generator());
		
	}

	public void StopGenerator()
	{
		StopAllCoroutines();
	}

	private IEnumerator Generator()
	{
		yield return new WaitForSeconds(delayToGenerate);

		int copiesGenerated = 0;

		do
		{
			GenerateItem();
			copiesGenerated++;
			yield return new WaitForSeconds(copiesInterval);

		} while (copiesGenerated < copiesToGenerate);

		if (loop)
			StartGenerator();
	}

	private void GenerateItem()
	{
		objectToGenerate.SpwObject(spawnPosition, Quaternion.identity);
		nextRepeteCount++;

	}


	private void SetOpenList()
	{
		for (int i = 0; i < spawnableAreas.Count; i++)
		{
			openList.Add(spawnableAreas[i]);
		}
	}
}
