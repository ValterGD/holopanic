﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class PersistentAudioPlayer : AudioPlayer
{
	private static PersistentAudioPlayer instance;

	[Header("Persistent System")]
	[SerializeField][Tooltip("Reinicia o audio tocado no momento que o playy for chamado quando a cena recarregar")]
	private bool restartOnReplay;

	private void Awake()
	{
		if (!instance)
		{
			instance = this;
			transform.parent = null;
			//Debug.Log("q?");
			DontDestroyOnLoad(gameObject);
		}
		else {
			Destroy(gameObject);
		}
	}

	public new void Play() {
		//Debug.Log("q");

		if (!restartOnReplay)
		{
			if (isPlaying)
			{
			//	Debug.Log("q");
				return;
			}
			else
			{
				base.Play();
				//Debug.Log("q");
			}
		}
		else {
			aS.Stop();
			base.Play();
		}
	}

}
