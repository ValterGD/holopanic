﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolAudioSource : MonoBehaviour
{

	[SerializeField] private GameObject audioPlayerPrefab;
	public CustomAudioClip customAudioClip;
	private Transform audioPoolTransform;
	private List<AudioPlayer> audioPool = new List<AudioPlayer>();
	[SerializeField] private int initialInstances;

	private void Start()
	{
		audioPoolTransform = new GameObject(gameObject.name + " - Audio Pool").transform;
		audioPoolTransform.parent = gameObject.transform;
		audioPoolTransform.localPosition = Vector3.zero;

		for (int i = 0; i < initialInstances; i++)
		{
			AddPlayer();
		}
	}

	private void AddPlayer()
	{
		audioPool.Add(Instantiate(audioPlayerPrefab, audioPoolTransform).GetComponent<AudioPlayer>());
		audioPool[audioPool.Count - 1].SetClip(customAudioClip);
	}

	public void Play()
	{
		for (int i = 0; i < audioPool.Count; i++)
		{
			if (!audioPool[i].isPlaying)
			{
				audioPool[i].SetClip(customAudioClip);
				audioPool[i].Play();
				return;
			}
		}

		AddPlayer();
		audioPool[audioPool.Count - 1].Play();
	}
}
