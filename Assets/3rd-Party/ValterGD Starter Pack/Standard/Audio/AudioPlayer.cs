﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioPlayer : MonoBehaviour
{

	[SerializeField]
	private CustomAudioClip customClip;
	[SerializeField] private bool playOnAwake;
	[SerializeField] private bool loop;
	protected AudioSource aS;

	public bool isPlaying { get { return aS.isPlaying; } }

	private void Start()
	{
		aS = GetComponent<AudioSource>();

		aS.loop = loop;

		if (playOnAwake) {
			Play();
		}
	}

	public void Play()
	{
		aS.clip = customClip.audioClip;
		aS.outputAudioMixerGroup = customClip.mixGroup;
		aS.volume = customClip.volume;
		aS.pitch = customClip.pitch;

		aS.Play();
	}

	public void SetClip(CustomAudioClip clip)
	{
		customClip = clip;
	}

}
