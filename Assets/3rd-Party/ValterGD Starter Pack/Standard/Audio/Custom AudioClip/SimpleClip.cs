using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[CreateAssetMenu(menuName = "ValterGD's System/Custom AudioClip/Simple", order = 0)]
public class SimpleClip : CustomAudioClip
{
	[SerializeField] private AudioClip audioClipFile;

	[SerializeField] [Range(0, 1)] private float _volume = 1;
	[SerializeField] [Range(-3, 3)] private float _pitch = 1;
	[SerializeField] private AudioMixerGroup audioMixGroup;

	public override AudioClip audioClip
	{
		get { return audioClipFile; }
	}

	public override float volume
	{
		get { return _volume; }
	}

	public override float pitch
	{
		get { return _pitch; }
	}

	public override AudioMixerGroup mixGroup
	{
		get
		{
			return audioMixGroup;
		}
	}
}