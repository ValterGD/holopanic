﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public abstract class CustomAudioClip : ScriptableObject
{
	private AudioClip _audioClip;

	public abstract AudioClip audioClip { get; }
	public abstract float volume { get; }
	public abstract float pitch { get; }

	public abstract AudioMixerGroup mixGroup { get; }

	public void ShootAudio()
	{
		AudioManager.ShootAudio(this);
	}
}