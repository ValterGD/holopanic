﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class BaseComponent : ScriptableObject
{
	public enum ComponentType { gun, especialist }

	[Header("Basic Data")]
	public string componentName;
	[ReadOnly] public ComponentType componentType;
}
