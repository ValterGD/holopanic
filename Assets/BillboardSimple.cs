﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BillboardSimple : MonoBehaviour
{
	[SerializeField] private Camera targetCamera;

	private void Awake()
	{
		if (targetCamera == null)
		{
			targetCamera = Camera.main;
		}
	}

	private void LateUpdate()
	{
		transform.localRotation = targetCamera.transform.rotation;
	}

}
