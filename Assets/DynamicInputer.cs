﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class DynamicInputer : MonoBehaviour
{
	[System.Serializable]
	public class DynamicInput
	{
		public string unityButtonName;
		public BaseComponent component;
		public Direction targetDirection;
	}

	[SerializeField][ListDrawerSettings(ShowIndexLabels = true, ListElementLabelName = "unityButtonName")]
	private List<DynamicInput> inputs;

	private CombatController cController;
	//private PlayerController mController;

	private void Awake()
	{
		cController = GetComponent<CombatController>();
	}

	private void Update()
	{
		DetectInput();
	}

	private void DetectInput()
	{
		foreach (DynamicInput dInput in inputs)
		{
			if (Input.GetButtonDown(dInput.unityButtonName))
				ExecuteAction(dInput);
			else if (Input.GetButtonUp(dInput.unityButtonName))
				CancelAction(dInput);
		}

	}

	private void ExecuteAction(DynamicInput input)
	{
		if(CheckIfIsAGun(input.component))
			cController.StartShooting(input.component as GunComponent, input.targetDirection);

	}

	private void CancelAction(DynamicInput input)
	{
		if (CheckIfIsAGun(input.component))
			cController.StopShooting(input.targetDirection);
	}

	public bool CheckIfIsAGun(BaseComponent component)
	{
		if (component.componentType == BaseComponent.ComponentType.gun)
			return true;
		else
			return false;
	}

}
