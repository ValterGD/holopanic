﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

[RequireComponent(typeof(Rigidbody))]
public class ProjectileController : MonoBehaviour, IPoolableObject
{
	[SerializeField] private float DefaultSpeed = 10;
	[SerializeField] private UnityEvent onSpawnedEvent;

	private Rigidbody rb;

	private void Awake()
	{
		rb = GetComponent<Rigidbody>();
	}

	public void OnSpawned()
	{
		rb.velocity = transform.forward * DefaultSpeed;
		onSpawnedEvent.Invoke();
	}

	public void Unspawn() { }
}
