﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnEnableUnityEvent : MonoBehaviour
{
	[SerializeField] private UnityEvent onEnableEvent, onDisableEvent;

	private void OnEnable()
	{
		onEnableEvent.Invoke();
	}

	private void OnDisable()
	{
		onEnableEvent.Invoke();
	}
}
