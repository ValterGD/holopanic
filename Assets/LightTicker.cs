﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightTicker : MonoBehaviour
{
	[SerializeField] private Light componentToTick;
	[SerializeField] private float tickDuration;

	public void TickComponent()
	{
		StopAllCoroutines();
		componentToTick.enabled = true;
		StartCoroutine(DisableComponent());
	}

	private IEnumerator DisableComponent()
	{
		yield return new WaitForSeconds(tickDuration);
		componentToTick.enabled = false;
	}

}
