﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ammo_", menuName = "Holo Panic/Combat/Ammunition", order = 1)]
public class Ammunition : ScriptableObject
{
	public ObjectPool pool;
}