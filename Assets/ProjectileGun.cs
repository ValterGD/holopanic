﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class ProjectileGun : MonoBehaviour
{
	[SerializeField] public ObjectPool ammoPool;

	public void AngledShoot(float angle)
	{
		Quaternion shootRotation = Quaternion.Euler(new Vector3(0, angle, 0));

		ammoPool.SpwObject(transform.position, shootRotation);
	}

}
