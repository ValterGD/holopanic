﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[RequireComponent(typeof(Rigidbody))]
public class AngledMovimentSystem : MonoBehaviour
{
	[SerializeField] [MinValue(0)] [Tooltip("Atenção! A velocidade de movimento e de projéteis tem fórmulas diferentes!")]
	private float _speed = 10;
	private float speed { get { return _speed / 100; } }

	private Rigidbody rb;

	private void Awake()
	{
		rb = GetComponent<Rigidbody>();
	}

	public void MoveAtAngle(float angle)
	{
		RotateAtAngle(angle);

		rb.MovePosition(transform.position + transform.forward * speed);
	}

	public void RotateAtAngle(float angle)
	{
		transform.localEulerAngles = new Vector3(0, angle, 0);
	}

	public void SetVelocityConstant()
	{
		rb.velocity = transform.forward * speed;
	}
	public void SetVelocityConstant(float speed)
	{
		_speed = speed;
		SetVelocityConstant();
	}
}
